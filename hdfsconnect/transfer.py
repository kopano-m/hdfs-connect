import subprocess
import pyreadr
import pandas as pd
import glob
import os
import numpy as np


class download:
    def __init__(self,hdfsDirData,file_name,localDir):
        self.hdfsDirData = hdfsDirData
        self.file_name = file_name
        self.localDir = localDir

    def get(self):
        subprocess.run(["hdfs", "dfs", "-get", self.hdfsDirData+"/"+self.file_name, self.localDir])


class upload:
    def __init__(self,localDir,current_file,hdfsDir):
        self.hdfsDir = hdfsDir
        self.current_file = current_file
        self.localDir = localDir

    def put(self):
        subprocess.run(["hdfs", "dfs", "-put","-f", self.localDir+"/"+self.current_file+" "+self.hdfsDir])

