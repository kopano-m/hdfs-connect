import os
import pandas as pd
import numpy as np
from tqdm import tqdm
import calendar
from hdfsconnect.transfer import download, upload
from hdfsconnect.getdf import create_df
import glob
import random

def Upload_to_HDFS(current_file, hdfsDir, localDir):
    """

    Upload data files to a remote hdfs folder

    Args:
        current_file (str) : path to the data file you want to upload

        hdfsDir (str) : path to the hdfs directory

        localDir (str) : path to the users home directory where the hdfs folder is hosted

    Returns:
        str : Where in hdfs the file is uploaded  

    """
    file = upload(localDir, current_file, hdfsDir)
    file.put()
    # Remove file in local directory once its transferred to hdfs
    os.remove(localDir+'/'+current_file)
    return print("File Uploaded successfully to "+hdfsDir)


def Download_From_HDFS(startYear, startMonth, no_of_months, IND, hdfsDirData, localDir, stopDay=None, stopMonth=None):

    """

    Download data files from a remote hdfs directory

    Args:
        startYear (bigint) : yyyy e.g 2021 (for year 2021)

        startMOnth (int) : M e.g (1 for January)

        no_of_months (int) : int e.g 6 (for 6 months) 

        IND (bool) : if True, then data for individuas will be downloaded, else entity data
                    will be downloaded.

        hdfsDirData (str) : path to the hdfs directory

        localDir (str) : path to the user's remote home directory

        stopDay (int): The day you want to stop downloading at (Optional Value)
        
        stopMonth (int): The month you want to stop downloading at (Optional Value) 

    Returns:
        df (pandas.DataFrame) : Dataframe of the data you want  

    """
    obj = calendar.Calendar()
    m = 1
    if IND == True:
        pbar = tqdm(no_of_months+1)
        while m <= no_of_months:
            if startMonth >= 13:
                print("Reached end of "+str(startYear)+"\n Moving to "+str(startYear+1)+"...")
                for (x,y) in obj.itermonthdays2(startYear+1, startMonth-12):
                    if x != 0:
                        file_name="daily_transactions_"+str(startYear+1)+str(startMonth-12).zfill(2)+str(x).zfill(2)+".dat"
            else:
                for (x,y) in obj.itermonthdays2(startYear, startMonth):
                    if x != 0:
                        if (x == stopDay) and (startMonth == stopMonth):
                            break
                        else:
                            file_name="daily_transactions_"+str(startYear)+str(startMonth).zfill(2)+str(x).zfill(2)+".dat"
                        file = download(hdfsDirData,file_name,localDir)
                        file.get()
            m += 1
            startMonth += 1
            pbar.update(1)
    elif IND == False:
        pbar = tqdm(no_of_months+1)
        while m <= no_of_months:
            if startMonth >= 13:
                print("Reached end of "+str(startYear)+"\n Moving to "+str(startYear+1)+"...")
                for (x,y) in obj.itermonthdays2(startYear+1, startMonth-12):
                    if x != 0:
                        file_name="daily_transactions_"+str(startYear+1)+str(startMonth-12).zfill(2)+str(x).zfill(2)+".dat"
            else:
                for (x,y) in obj.itermonthdays2(startYear, startMonth):
                    if x != 0:
                        if (x == stopDay) and (startMonth == stopMonth):
                            break
                        else:
                            file_name="daily_transactions_"+str(startYear)+str(startMonth).zfill(2)+str(x).zfill(2)+".dat"
                        file = download(hdfsDirData,file_name,localDir)
                        file.get()
            m += 1
            startMonth += 1
            pbar.update(1)
        pbar.close()

    files = [file_name for file_name in glob.iglob(localDir+'/*.dat', recursive=True)]
    print("\nAll "+str(len(files))+" .dat files successfully retrieved from "+hdfsDirData)
    df = create_df(localDir, files).create()
    print("\nDataframe successfully created! Remember to save it if you're going to re-use it :)")
    return df


def encrypt_account_no(account_number):
    """
    Encrypt account number

    Args:
        account_number (str): the account number you want to encrypt

    Returns:
        encrypted_acount_no (str): the encrypted account number
    """
    # Shuffle the list ls using the seed `seed`
    ls = [int(x) for x in str(account_number)]
    random.seed(5)
    random.shuffle(ls)
    shuffled_str="".join([str(i) for i in ls])
    return shuffled_str


def unencrypt_account_no(encypted_account_no):
    """
    Unencrypt account numbers

    Args:
        encrypted_account_no (str): Encrypted account number

    Return:
        unencrypted_account_no (str): original account number
    """
    shuffled_ls = [int(x) for x in str(encypted_account_no)]
    n = len(shuffled_ls)
    # Perm is [1, 2, ..., n]
    perm = [i for i in range(1, n + 1)]
    # Apply sigma to perm
    def shuffle(ls):
    # Shuffle the list ls using the seed `seed`
        random.seed(5)
        random.shuffle(ls)
        return ls
    shuffled_perm = shuffle(perm)
    # Zip and unshuffle
    zipped_ls = list(zip(shuffled_ls, shuffled_perm))
    zipped_ls.sort(key=lambda x: x[1])
    unshuffled_ls = [a for (a, b) in zipped_ls]
    unshuffled_str = "".join([str(i) for i in unshuffled_ls])
    return unshuffled_str