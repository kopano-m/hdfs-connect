import pyreadr
import pandas as pd
import glob
import os
import numpy as np
from tqdm import tqdm

class create_df:
    def __init__(self,localDir,files):
        self.localDir = localDir
        self.files = files

    def create(self):
        print(str(len(self.files))+" transactions downloaded. Concatenating them into one file, this might take a while...")
        df_from_each_file = (pd.read_csv(file_name) for file_name in tqdm(self.files,total=len(self.files)))
        concatenated_df   = pd.concat(df_from_each_file, ignore_index=True)
        print("\nDeleting .dat files...")
        for file_name in self.files:
            os.remove(file_name)
        self.concatenated_df = concatenated_df
        return self.concatenated_df
        