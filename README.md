# HDFSConnection


## Installation

Use package manager [pip](https://pip.pypa.io/en/stable/) to install the hdfs connection package.
```bash
$ pip install git+https://kopano-m@bitbucket.org/kopano-m/hdfs-connect.git
```

## Update

```bash
$ pip install --upgrade git+https://kopano-m@bitbucket.org/kopano-m/hdfs-connect.git
```

## Usage

```bash
from hdfsconnect import datafile
```

### Uploading to HDFS

```bash

datafile.Upload_to_HDFS(**Args)
    """
    Takes in user credentials, file source and destination.
    Returns a confirmation of the upload.

    Args:
        current_file (str) : path to the data file you want to upload

        hdfsDir (str) : path to the hdfs directory

        localDir (str) : path to the users home directory where the hdfs folder is hosted

    Returns:
        str : Confirmation of the upload  

    """
```
### Downloading file from HDFS

```bash

datafile.Download_From_HDFS(**Args):

    """

    Download data files from a remote hdfs directory

    Args:
        out_file (str) : name of your output file

        startYear (bigint) : yyyy e.g 2021 (for year 2021)

        startMOnth (int) : M e.g (1 for January)

        no_of_months (int) : int e.g 6 (for 6 months) 

        IND (bool) : if True, then data for individuas will be downloaded, else entity data
                    will be downloaded.

        hdfsDirData (str) : path to the hdfs directory

        localDir (str) : path to the user's remote home directory

        stopDay (int): The day you want to stop downloading at (Optional Value)
        
        stopMonth (int): The month you want to stop downloading at (Optional Value) 

    Returns:
        df (pandas.DataFrame) : Dataframe of the data you want  

    """
```

### Encrypting Account Numbers
```
df['ENCRYPTED_ACCOUNT_NO'] = df['ACCOUNT_NUMBER'].apply(datafile.encrypt_account_no)

```
### Unecrypt Account Numbers
```
df['ORIGINAL_ACCOUNT_NO'] = df['ENCRYPTED_ACCOUNT_NO'].apply(datafile.unencrypt_account_no)

```


## Limitations
Package is still work in progress. it can only download 2 years worth of data. Starting day is always the 1st of your starting Month.

## Contributing
Pull requests are welcome. For major changes, please open an issue to discuss what you would like to change or add.

## License
[MIT](https://choosealicense.com/license/mit/)

## Authors
Kopano Monyobo <br>kopano.monyobo@absa.africa<br/>