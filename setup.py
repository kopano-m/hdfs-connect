from setuptools import setup, find_packages

setup(
    name='hdfsconnect',
    version='3.0',
    packages=find_packages(exclude=['tests*']),
    license='MIT',
    description='',
    long_description=open('README.md').read(),
    install_requires=['numpy', 'pandas', 'tqdm', 'pyreadr'],
    url='https://kopano-m@bitbucket.org/kopano-m/hdfs-connect.git',
    author='Kopano Monyobo',
    author_email='kopano.monyobo@absa.africa'
)